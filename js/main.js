/*
	Joe's School of Learning
	Author: Joseph Cavagna
*/

	/*
	===============================================
	=============================FLASH MEDIA PLAYER
	*/

var flashReady = function(){

	/*
	===============================================
	=========================== FLASH PLAYER LOADED
	*/

// Setting hard variables for the arrays when loaded instead of calling on the arrays for every call
	var miclist = flash.getMicrophones();
	var camlist = flash.getCameras();

// Default states of the selectors to be shown when activated in the media player
	$('#selectmic').hide();
	$('#selectcam').hide();

// Sets the connected items in a list to be selected to set the camera and microphone
	$.each(miclist, function(i, item){
			$('#selectmic').add("<h3 class='micnum' index='" + i + "' style='display:block'>" + item + "</h3>").appendTo('#selectmic');
		});
	$.each(camlist, function(i, item){
			$('#selectcam').add("<h3 class='camnum' index='" + i + "' style='display:block'>" + item + "</h3>").appendTo('#selectcam');
		});

	/*
	===============================================
	=========================== FLASH PLAYER EVENTS
	*/

// Play Button
	var isPlay = 0;
	$('#play').on('click', function(e){
		// Check to ensure record is not happening, sets it to stop if it is, resets the state
		if(isRecord != 0){
			flash.stopRecording();
			isRecord = 0;
			$('#record').attr('class', 'inactive');
		}
		// If play hasn't been clicked, it sets the status to active and makes the connection
		if(isPlay == 0){
			$('#play').attr('class', 'active');
			flash.connect('rtmp://localhost/SMSServer');
			isPlay = 2;
		// If the connection is already made, it toggles the start and stop of the playback
		}else if(isPlay == 1){
			flash.playPause();
			isPlay = 2;
			$('#play').attr('class', 'active');
		// This is the pause function of the toggle. Sets it back to state where the video is loaded
		}else{
			flash.playPause();
			isPlay = 1;
			$('#play').attr('class', 'inactive');
		}

		e.preventDefault();
	});

// Record Button
	var isRecord = 0;
	$('#record').on('click', function(e){
		// Checks to ensure the playback isn't active. If it is, then it stops it and resets the state
		if(isPlay != 0){
			flash.stopPlaying();
			isPlay = 0;
			$('#play').attr('class', 'inactive');
		}
		// Establishes the connection for the recording
		if(isRecord == 0){
			$('#record').attr('class', 'active');
			flash.connect('rtmp://localhost/SMSServer');
			isRecord = 1;
		// Stops the recording.
		}else{
			flash.stopRecording();
			isRecord = 0;
			$('#record').attr('class', 'inactive');
		}

		e.preventDefault();
	});

// Set Time
	$('#progress').on('click', function(e){
		// When clicked this moves the movie to the time clicked on the bar Offset and length calculates the position
		var whereclick = e.pageX - $(this).offset().left;
		flash.setTime(whereclick / 225 * vidLength);

		e.preventDefault();
	});

// Set Volume
	$('#volume').on('click', function(e){
		// This mimics the progress bar where the click sets the level based on percentage. This also sets the level based on that amount
		var thereclick = e.pageX - $(this).offset().left;
		flash.setVolume(thereclick / 35);
		var xvol = (flash.getVolume()*35) + 'px';
		$("#level").css('width', xvol);

		e.preventDefault();
	});

// Set Mic View Toggle
	var isMic = 0;
	$('#microphone').on('click', function(e){
		// If the cam selector is open, it closes and hides it.
		if(isCam != 0){
			$('#selectcam').hide();
			isCam = 0;
			$('#webcam').attr('class', 'inactive');
		}

		if(isMic == 0){
			// shows the mic list if closed on click
			$('#selectmic').show();
			isMic = 1;
			$('#microphone').attr('class', 'active');
		}else{
			// hides the mic list on click, this doubles to close when the selection is made as well because they exist inside this div
			$('#selectmic').hide();
			isMic = 0;
			$('#microphone').attr('class', 'inactive');
		}
	});

// Set Mic
	$('.micnum').on('click', function(e){
		// Takes the attribute from the index of the array that is set to an attribute on the object and sets it to the selected global variable
		microphoneSelected = $(this).attr('index');

		e.preventDefault();
	});

// Set Cam View Toggle
	var isCam = 0;
	$('#webcam').on('click', function(e){
		// If the mic selection is open, it shuts it before opening the camera selection
		if(isMic != 0){
			$('#selectmic').hide();
			isMic = 0;
			$('#microphone').attr('class', 'inactive');
		}

		if(isCam == 0){
			// Opens the camera selection list
			$('#selectcam').show();
			isCam = 1;
			$('#webcam').attr('class', 'active');
		}else{
			// Shuts the camera selection whenver the button is clicked again or a selection is made
			$('#selectcam').hide();
			isCam = 0;
			$('#webcam').attr('class', 'inactive');
		}
	});

// Set Cam
	$('.camnum').on('click', function(e){
		// Sets the global camera to the camera selected in the list via the index attribute
		cameraSelected = $(this).attr('index');
		e.preventDefault();
	});

};

	/*
	===============================================
	================================= FLASH GLOBALS
	*/

var cameraSelected = 0,
	microphoneSelected = 0,
	currentTime = 0,
	vidLength = 0,

	connected = function(success,error){
		// Using logic to see which is currently being selected, play or record.
		if($('#play').attr('class') == 'active'){
			flash.startPlaying('hobbit_vp6.flv');
			flash.setVolume(0.71);
		}

		if($('#record').attr('class') == 'active'){
			flash.startRecording('recorded.flv', cameraSelected, microphoneSelected);
		}

	},
	seekTime = function(time){
		// Using the time from the video to set the width of the played bar overlaying the player
		var xpos = (time/vidLength * 225) + 'px';
		currentTime = xpos/225 * vidLength;
		$("#played").css('width', xpos);
	},
	getDuration = function(duration){
		// Calculates the length of the media being played
		vidLength = duration;
	},
	recordingError = function(message,code){},
	globalError = function(message){};

	/*
	============================== END FLASH PLAYER
	===============================================
	*/

$(document).ready(function(){

	/*
	===============================================
	========================== SIMPLE LOGIN SECTION
	*/
	// Defaults to set variables from the different login servers
	var userpic = '';
	var name = '';
	var chatRef = new Firebase('https://blazing-fire-6418.firebaseio.com');
	var auth = new FirebaseSimpleLogin(chatRef, function(error, user) {
		if (error) {
	    // an error occurred while attempting login
	    	console.log(error);
		}else if(user){
	    // user authenticated with Firebase
	    	name = user.name;
	    	showbox();
	   		console.log('User ID: ' + user.id + ', Provider: ' + user.provider);
	    	if(user.provider == 'facebook'){
	    		userpic = 'http://graph.facebook.com/'+user.id+'/picture';
	    	}else if(user.provider == 'twitter'){
	    		userpic = user.profile_image_url;
	    	}
	  	}else{
	    // user is logged out
	  	}
	});
	// Logs in with facebook using simplelogin from firebase, hides the login options.
	$('#FBLoginLink').on('click', function(e){
		auth.login('facebook');
		$('.logout').show();
		$('.login').hide();

		e.preventDefault();
	});
	// Logs in with twitter
	$('#TwitterLoginLink').on('click', function(e){
		auth.login('twitter');
		$('.logout').show();
		$('.login').hide();

		e.preventDefault();
	});

	// Whenever the logout is clicked, it hides the message box shows the login selector and sends the logout command to firebase
	$('#LogoutLink').on('click', function(e){
		auth.logout();
		$('.logout').hide();
		$('.login').show();
		$('#addCom').hide();

		e.preventDefault();
	});
	/*
	============================== END SIMPLE LOGIN
	===============================================
	*/

	/*
	===============================================
	============================== COMMENTS SECTION
	*/

	// Connection to the firebase server
    var myDataRef = new Firebase('https://blazing-fire-6418.firebaseio.com/SMS/');

    // Whenever a message is added to the database, it is also added to the screen
    myDataRef.on('child_added', function(snapshot) {
    	var message = snapshot.val();
    	displayChatMessage(message.name, message.text, message.picture);
   	});

   	function displayChatMessage(name, text, picture) {
        $('<div/>').html('<p>' + text +'</p>').prepend($('<em/>').html('<img class="userpic" src="'+picture+'">'+name+': ')).appendTo($('#messagesDiv'));
    };
    // Whenever the function is enacted the box for comments is shown and the listener for enter in the message field is added
	showbox = function (){
		$('#addCom').show();
		$('#messageInput').keypress(function(e){
	    	if (e.keyCode == 13) {
	    		console.log("enter hit");
	    		var text = $('#messageInput').val();
	    		var picture = userpic;
	    		myDataRef.push({name: name, text: text, picture: picture});
	    		$('#messageInput').val('');
	    	}
	 	});
	}
	// Set the default fields to hide
	$('.logout').hide();
	$('#addCom').hide();
	/*
	========================== END COMMENTS SECTION
	===============================================
	*/

});





